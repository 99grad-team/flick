# Clone of Flickity

_Touch, responsive, flickable carousels_

See [flickity.metafizzy.co](https://flickity.metafizzy.co) for complete docs and demos.

This is an experimental fork of the flickity slider by metafizzy.

By [Metafizzy 🌈🐻](https://metafizzy.co)
